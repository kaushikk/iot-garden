#ifndef sensor_h
#define  sensor_h

#include <Arduino.h>
#include <Adafruit_Sensor.h>


//library 19 DHT
#include <DHT.h>
//library 18 DHT
//#include <DHT_U.h>



class sensor {
public:
sensor(bool debugEnabled);
int motorlControls();
int readwaterlevel();
int readmoisture();
float readtempreture();
float readhumidity();

private:
bool debugOn;

};

#endif
