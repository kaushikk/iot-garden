
#include "sensor.h"


//All Sensor pin configeration

//sonic sensor
static int trig = 5; // attach D1 to Trig
static int echo = 4; //attach D2 to Echo

//moisture sensor
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
//define miminum analog value
//moisture sensor given 480 in water.
static int soilminimumvalue = 520;



static int motorcontrol =14; // attach control pin to D5


// DHT22 tempreture and humidity
#define DHTPIN            2         // Pin D4 which is connected to the DHT sensor.
// Uncomment the type of sensor in use:
//#define DHTTYPE           DHT11     // DHT 11
#define DHTTYPE           DHT22     // DHT 22 (AM2302)
//#define DHTTYPE           DHT21     // DHT 21 (AM2301)
DHT dht(DHTPIN, DHTTYPE);
//DHT_Unified dht(DHTPIN, DHTTYPE);

//sonic sensor calculation
long ReadSonicSensor();

sensor::sensor(bool enableDebug) {
    debugOn = enableDebug;
}


int sensor::motorlControls(){
  return motorcontrol;

}

float sensor::readhumidity(){

        dht.begin();

        delay(2000);

        float h = dht.readHumidity();
        while (isnan(h)) {
                h = dht.readHumidity();
        }
        return h;
}

float sensor::readtempreture(){
        // Initialize device.
        dht.begin();
        delay(2000);
        // Read temperature as Fahrenheit (isFahrenheit = true)
        float f = dht.readTemperature(true);
//keep reading until get a valid value.
        while (isnan(f)) {
                f = dht.readTemperature(true);
        }

        return f;
}



int sensor::readmoisture(){
        //number of samples
        int samples = 5; int tolorent = 10; int count = 0;

        int sensorValue[samples];  // value read from the pot
        int outputValue = 0;  // value output to the PWM (analog out)
        int moisturelevel =0;

        while(count<samples-1) {
                for (int i=0; i<samples; i++) {
                        delay(300);
                        // read the analog in value:
                        sensorValue[i] = analogRead(analogInPin);
                        // Serial.print("moisture sensor value: ");
                        // Serial.println(sensorValue[i]);
                }
                count = 0;
                outputValue = sensorValue[0];
                for(int i=1; i<samples; i++) {
                        //tolorent number
                        if(abs(sensorValue[i]-outputValue)<=tolorent) {
                                outputValue = (sensorValue[i]+outputValue) / 2;
                                // Serial.print("moisture Output value: ");
                                // Serial.println(outputValue);
                                count++;
                        }
                        else  break;
                }
                //end reading sensor data.
        }
        // map it to the range of the analog out:
        moisturelevel = map(outputValue, soilminimumvalue, 1023, 100, 0);
        return moisturelevel;



}
int sensor::readwaterlevel(){
        //number of samples
        int samples = 5; int tolorent = 10; int count = 0;
        long waterlevel[samples]; long waterlevelr=0;

        while(count<samples-1) {
                for (int i=0; i<samples; i++) {
                        delay(300);
                        waterlevel[i] = ReadSonicSensor();

                }
                count = 0;
                waterlevelr = waterlevel[0];
                for(int i=1; i<samples; i++) {
                        //tolorent number
                        if(abs(waterlevel[i]-waterlevelr)<=tolorent) {
                                waterlevelr = (waterlevel[i]+waterlevelr) / 2;
                                count++;
                        }
                        else  break;
                }
                //end reading sensor data.
        }
        return (int) waterlevelr;
}


long ReadSonicSensor(){
        // establish variables for duration of the ping,
        // and the distance result in inches and centimeters:
        long duration, inches, cm;

        // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
        // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
        pinMode(trig, OUTPUT);
        digitalWrite(trig, LOW);
        delayMicroseconds(2);
        digitalWrite(trig, HIGH);
        delayMicroseconds(5);
        digitalWrite(trig, LOW);

        // The same pin is used to read the signal from the PING))): a HIGH
        // pulse whose duration is the time (in microseconds) from the sending
        // of the ping to the reception of its echo off of an object.
        pinMode(echo,INPUT);
        duration = pulseIn(echo, HIGH);

        // convert the time into a distance
        // According to Parallax's datasheet for the PING))), there are
        // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
        // second). This gives the distance travelled by the ping, outbound
        // and return, so we divide by 2 to get the distance of the obstacle.
        // See: http://www.parallax.com/dl/docs/prod/acc/28015-PI...
        inches = duration / 74 / 2;
        // The speed of sound is 340 m/s or 29 microseconds per centimeter.
        // The ping travels out and back, so to find the distance of the
        // object we take half of the distance travelled.
        cm =    duration / 29 / 2;

        //print output.
        // Serial.print(inches);
        // Serial.print("in, ");
        // Serial.print(cm);
        // Serial.print("cm");
        // Serial.println();

        return cm;
}
