/**
Utilities Method for IOT Garden

*/
#ifndef util_h
#define  util_h

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>

class Util {
public:
    Util(bool debugEnabled);
    JsonObject& sendJson(String url,String payload, JsonBuffer& buff);
    String httpGet(String url);
    JsonObject& jsonGet(String url, JsonBuffer& buff);
    void debug(String s);
    IPAddress connectWifi(const char* ssid, const char* password);
    IPAddress getLocalIP();
    void test();
    String hello();
    String getMac();
    //JsonObject& jsonFromThing(Thing& t, JsonBuffer& jb);
private:
    HTTPClient _http;
    bool debugOn;
    ESP8266WebServer _server;
};

const int THING_WATER_LEVEL = 0 ;
const int THING_TEMPERATURE = 1;
const int THING_SOIL = 2;

class Thing {
    int id;
    String ip;
    bool types[3];
    String mac;
public:
    Thing() {

    }

    Thing(int _id,String _ip, int type) {
        types[0]=false;
        types[1]=false;
        types[2]=false;
        id = _id;
        ip = _ip;
        if(type>=0 && type<=2)
            types[type]=true;
    }

    int getId() {
        return id;
    }

    String getIP() {
        return ip;
    }

    void setSoil(bool b) {
        types[THING_SOIL]=b;
    }

    void setMac(String s) {
        mac = s;
    }

    String getMac() {
        return mac;
    }

    bool isSoil() {
        return types[THING_SOIL]==true;
    }

    void setWater(bool b) {
        types[THING_WATER_LEVEL];
    }

    bool isWater() {
        return types[THING_WATER_LEVEL]==true;
    }

    void setTemp(bool b) {
        types[THING_TEMPERATURE];
    }

    bool isTemp() {
        return types[THING_TEMPERATURE]==true;
    }
};

#endif
