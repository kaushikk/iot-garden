#include "util.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>



Util::Util(bool enableDebug) {
    debugOn = enableDebug;
}

String Util::getMac() {
        byte mac[6];
        WiFi.macAddress(mac);
        String s = String(mac[5],HEX)+":"+String(mac[4],HEX)+":"+String(mac[3],HEX)+":"+String(mac[2],HEX)
        +":"+String(mac[0],HEX)+":"+String(mac[0],HEX);
        return s;
}

IPAddress Util::connectWifi(const char *ssid, const char *password) {
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      debug(".");
    }
    debug("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    return WiFi.localIP();
}

IPAddress Util::getLocalIP() {
    return WiFi.localIP();
}

/*
Send a JSON through HTTP POST. Returns value as JSON
Content-Type: application/json
Accepts: application/json
*/
JsonObject& Util::sendJson(String url, String payload, JsonBuffer& _jb){
    HTTPClient c;
    String response="";
    c.begin(url);
    c.addHeader("Content-Type", "application/json");
    debug("POST: url -  "+url);
    // debug("POST: payload: "+payload);
    int statusCode = c.POST(payload);
    if (statusCode > 0) {
        Serial.print("POST: Status: ");
        Serial.println(statusCode);
        if (statusCode == 200) {
            response = c.getString();
            Serial.println("POST: response: "+response);

        }else {
            Serial.println("RESULT NOT 200...");
        }
        c.end();
        JsonObject& o = _jb.parseObject(response);

        if(debugOn==true) {
            Serial.println("JSON Output: ");
            o.printTo(Serial);
        }

        return o;
    }else {
      Serial.println("POST: status error!"+statusCode);
    }
    return _jb.createObject();
}

/*
HTTP Get with no query parameters. Returns a String response
*/
String Util::httpGet(String url) {
    StaticJsonBuffer<200> _jb;
	HTTPClient c;
    String response="";
    c.begin(url);
    debug("connecting to url: "+url);
    int statusCode = c.GET();
    if (statusCode > 0) {
      debug("Success");
      if (statusCode == HTTP_CODE_OK) {
        debug("Received: " + statusCode);
        response = c.getString();
    }else {

    }
    c.end();
    //parse the JSON from response
    debug("response: "+response);
  }else {
    Serial.print("status code: ");
    Serial.println(statusCode);
  }
  return response;
}

/*
    HTTP Get that returns a JSON from server
*/
JsonObject& Util::jsonGet(String url,JsonBuffer&  _jb) {
    String s = httpGet(url);
    return _jb.parseObject(s);
}

void Util::debug(String s) {
    Serial.println(s);
}

String Util::hello() {
    char json[] =
          "{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";
          StaticJsonBuffer<200> _jb;
    JsonObject& root = _jb.parseObject(json);

    return String("HELLO!!!");
}

/*
Test method that POSTs JSON request to Server
*/
void Util::test() {
    String registerUrl = "http://172.20.96.221:8000/registration";
    String url = "http://172.20.96.221:8000/";
    StaticJsonBuffer<200> jb;
    JsonObject& obj = jsonGet(url,jb);
    const char* name;
    long age;
    if(!obj.success()) {
        debug("failed to parse JSON");
    }else {
        name = obj.get("name");
        age = obj.get("age");
        Serial.println(name);
        Serial.println(age);
        String s;
        obj.printTo(s);
        StaticJsonBuffer<800> jb1;
        JsonObject& o1 = sendJson(registerUrl, s, jb1);
        if(o1.measureLength()>0) {
            o1.prettyPrintTo(Serial);
        }else {
            debug("Error: JSON Object is NULL");
        }
    }
}

// JsonObject& Util::jsonFromThing(Thing& t, StaticJsonBuffer& jb) {
//     JsonObject& o = jb.createObject();
//     o["deviceId"]=t.getId();
//     o["localIP"]=t.getIP();
//     JsonObject& cap = o.createNestedObject("capabilities");
//     if(t.isTemp()) {
//         JsonObject& temp = cap.createNestedObject("temperature");
//         temp["remoteControl"]="false";
//         temp["threshold"]="false";
//     }
//     if(t.isSoil()) {
//         JsonObject& temp = cap.createNestedObject("soil");
//         temp["remoteControl"]="false";
//         temp["threshold"]="false";
//     }
//     if(t.isTemp()) {
//         JsonObject& temp = cap.createNestedObject("waterlevel");
//         temp["remoteControl"]="false";
//         temp["threshold"]="false";
//     }
//     return o;
// }
