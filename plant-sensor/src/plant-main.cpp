#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <util.h>
#include <sensor.h>

String serverName = "http://172.20.96.221:3000";
String registerUrl = serverName+"/registration";
String sendUrl = serverName+"/data";
// String registerUrl = "http://172.20.96.221:8000/registration";
const char* ssid="BroncoWiFi";
const char* pwd="gosantaclara";

bool _debug=true;
Util util(_debug);
sensor _sensor(true);

// Webserver , Local IP Address
IPAddress ip;
ESP8266WebServer server(80);
//Buffer of
StaticJsonBuffer<4096> _devBuff;
Thing _self;
Thing _water;
Thing _temperature;


//moisture threshold value
static int moisturelow = 20;
static int moisturewater = 40;
static int moistureok = 60;
//weather type factor
int weatherfactor = 0;
//waterlevel varibale
int limitwaterlevel = 10;
//hight temperature
int hightemperature = 88;

//water duration
int duration=5000;
bool watered=false;

void irrigatePlant(int watertime){
  watered=true;
  digitalWrite(_sensor.motorlControls(), HIGH);
  delay(watertime);
  digitalWrite(_sensor.motorlControls(),LOW);

}

void connectCloud() {
    StaticJsonBuffer<400> jb;
    JsonObject& o = jb.createObject();
    o["deviceId"]=_self.getId();
    o["localIP"]=_self.getIP();
    o["mac"]=_self.getMac();
    JsonObject& cap = o.createNestedObject("capabilities");
    if(_self.isTemp()==true) {
        JsonObject& temp = cap.createNestedObject("temperature");
        temp["remoteControl"]="false";
        temp["threshold"]="false";
    }
    if(_self.isWater()==true) {
        JsonObject& temp = cap.createNestedObject("waterlevel");
        temp["remoteControl"]="false";
        temp["threshold"]="false";
    }
    if(_self.isSoil()==true) {
        JsonObject& temp = cap.createNestedObject("soil");
        temp["remoteControl"]="false";
        temp["threshold"]="false";
    }
    String payload;
    o.printTo(payload);
    util.debug("device info: ");
    o.prettyPrintTo(Serial);
    //How to store this information in a varaiable? and access in loop method?
    JsonObject& deviceList = util.sendJson(registerUrl,payload, _devBuff);
    if(deviceList.measureLength()>0) {
        util.debug("deviceList: ");
        deviceList.prettyPrintTo(Serial);
        JsonArray& details = deviceList.get("details");
        int size = details.size();
        if(_self.isSoil()==true) {
            for(int i=0;i<size;i++ ) {
                JsonObject& device = details.get(i);
                int _id = device.get("deviceId");
                if(_id==THING_TEMPERATURE) {
                    _temperature = Thing(THING_TEMPERATURE, device["localIP"],THING_TEMPERATURE);
                }else if(_id==THING_WATER_LEVEL) {
                    _water = Thing(THING_WATER_LEVEL, device["localIP"],THING_WATER_LEVEL);
                }
            }
        }
    }else {
        util.debug("error getting device List");
    }
}


void setup() {
    Serial.begin(115200);
    util.debug("starting iot...plant-sensor...");
    ip = util.connectWifi(ssid, pwd);
    _self = Thing(THING_SOIL,ip.toString(),THING_SOIL);
    _self.setMac(util.getMac());
    connectCloud();
    pinMode(_sensor.motorlControls(),OUTPUT);
    digitalWrite(_sensor.motorlControls(),LOW);
    //server = util.startWebServer();
}

void loop() {
    watered = false;
    util.debug("plant-sensor....");
    if(_self.isSoil()==true) {
        Serial.println("Temperature Sensor: "+_temperature.getIP());
        Serial.println("Water Sensor: "+_water.getIP());
    }
    util.debug("update new address table");

    //fecthing live data from ESP8266
    util.debug("fectch new data");
    String temp  = util.httpGet("http://" + _temperature.getIP() + "/temp/get");
    util.debug("temp: "+temp);
    float temperature =  temp.toFloat();
    Serial.println(temperature);
    String waterlvl = util.httpGet("http://" + _water.getIP() + "/water/get");
    util.debug("water: "+waterlvl);
    int waterlevel = waterlvl.toInt();
    Serial.println(waterlevel);
    int currentmoisture = _sensor.readmoisture();

    Serial.print("Moisture: ");
    Serial.println(currentmoisture);
    //Irrigatation algo
    //plant is in critical state
    if(currentmoisture<=moisturelow){ //low moisture
      util.debug("very dry..override irrigate");
      if(waterlevel>limitwaterlevel){
        util.debug("Water Level: Critical......empty....Not Irrigating");
        delay(5000);
        return;
    }else {
      irrigatePlant(duration);
    }

  } else if(currentmoisture>moisturelow&&currentmoisture<=moisturewater){ // medium moisture
      util.debug("not very dry...make irrigation decision");
      //checking moisture
      //checking waterlevel
      //checking weater
      //checking tempreture whether is high or normal
      if(waterlevel>limitwaterlevel){
        util.debug("water level: Sufficient....ok to water, checking weather");
        if(weatherfactor==1){
            util.debug("weather check? gonna rain, avoiding overwater");
            // delay(5000);
            // return;
        }else if(temperature>=hightemperature){
          util.debug("Temperature: Too hot..Wait the temerature goes down");
        //   delay(5000);
        //   return;
      }else{ ////ok to water
          util.debug("Irrigating Plant......");
          irrigatePlant(duration);
        }
      }
      else if(waterlevel<=limitwaterlevel){
        util.debug("Water Level: Critical...save the water for critical plant");
        // delay(5000);
        // return;
    }else {
        util.debug("***Irrigation Decision - Undecided**");
    }
    //plant is ok
} else if(currentmoisture>moisturewater){
      util.debug("Plant is ok");
    //   delay(5000);
  }else {

      util.debug("Overwatering....");
  }
  StaticJsonBuffer<400> buff;
  JsonObject& obj = buff.createObject();
  JsonObject& dObj = obj.createNestedObject("data");
  dObj["soil"] = currentmoisture;
  dObj["temp"] = temperature;
  dObj["waterlevel"] = waterlevel;
  obj["deviceId"] = _self.getId();
  obj["waterduration"] = duration;
  obj["watered"] = watered;
  String payload;
  obj.printTo(payload);
  util.debug("sending data: ");
  obj.prettyPrintTo(Serial);
  //How to store this information in a varaiable? and access in loop method?
  JsonObject& ret = util.sendJson(sendUrl,payload, _devBuff);
  util.debug("response: ");
  ret.prettyPrintTo(Serial);
  delay(3000);
}
