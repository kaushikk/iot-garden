README

Node.JS Backend:

**Commands to run once you chekcout this project :**

Assuming you have installed Node.JS ,npm on your computer.

Run following commands once you are inside Node backend project

```
#!sh

  npm install   (This will download and install all necessary modules for this project)
  
  node app.js   (This will start your Node Server on port 3000.)
  

```

Test :

  Try to run following url from your browser which should display JSON String on your browser
  
  http://localhost:3000/
  
  
**Main Modules used :**

1. Express
2. Monk (MongoDB)
3. request
4. Body-Parse (To Parse JSON request)

To start Server : node app.js

URL to post your things data : http://localhost:3000/registration

**Expected parameters :** 

Any JSON

e.g.   

```
#!JSON

{
   "deviceId": "1",
    "localIP": "10.0.0.1",
    "capabilities": {
      "soil": {
        "remoteControl": "false",
        "thresholdControl": "false"
      },
      "temperature": {
        "remoteControl": "false",
        "thresholdControl": "false"
      },
      "waterlevel": {
        "remoteControl": "false",
        "thresholdControl": "false"
      }
}
```



**Response JSON** 

Following JSON is expected as response to your request.


```
#!JSON

[
  {
    "_id": "56e66679e93da2636cc02c00",
    "deviceId": "1",
    "localIP": "10.0.0.1",
    "capabilities": {
      "soil": {
        "remoteControl": "false",
        "thresholdControl": "false"
      },
      "temperature": {
        "remoteControl": "false",
        "thresholdControl": "false"
      },
      "waterlevel": {
        "remoteControl": "false",
        "thresholdControl": "false"
      }
    },
   "ttl": 1458075210
  },
  {
    "deviceId": "2",
    "localIP": "10.0.0.2",
    "capabilities": {
      "soil": {
        "remoteControl": "false",
        "thresholdControl": "false"
      },
      "temperature": {
        "remoteControl": "false",
        "thresholdControl": "false"
      },
      "waterlevel": {
        "remoteControl": "false",
        "thresholdControl": "false"
      }
    },
    "ttl": 1458075219,
    "_id": "56e87653f88fb8b436000001"
  }
]
```


You can igonore ttl : It is time to leave for Thing. It is used by Node Server to check whether thing is alive or not.