
/**
 * Module dependencies.
 */

var express = require('express'), stylus = require('stylus')
  , nib = require('nib');

var routes = require('./routes');
var collection = require('./routes/registeration');
var update = require('./routes/updateThings');
var weather = require('./routes/weather');
var plantData = require('./routes/data');


var http = require('http');
var path = require('path');
var ui = require('./routes/ui');

var app = express();

function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .use(nib());
}


var bodyParser = require('body-parser');
//Mongo DB Connections

  var monk = require('monk');
  var db = monk('mongodb://ruchir:ruchir1987@oceanic.mongohq.com:10016/nextglide_staging?replicaSet=set-530ce7bd23ff0a607800006f');

 // var db = monk('http://localhost:27017/expresshogan/');
  //app.db=db;
// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
// app.use(require('less-middleware')({ src: path.join(__dirname, 'public') }));
app.use(stylus.middleware(
  { src: __dirname + '/public'
  , compile: compile
  }
))
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.options('/*', function( query, reply ) {
reply.writeHead( '204', {
'Access-Control-Allow-Origin': '*', // allow x-domain requests
'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, OPTIONS', // enable all http methods
'Access-Control-Allow-Credentials': false,
'Access-Control-Max-Age': '86400', // allow caching of this response
'Access-Control-Allow-Headers': 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
} );
reply.end();
} );

app.all('/*',function(req,res,next) {

  res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization');
   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
   // res.header('Content-Type', 'application/json');
	next();
});

app.get('/', routes.index(db));

app.post('/data',plantData.insertPlants(db));
app.get('/updateThings',update.updateById(db));
app.get('/weather',weather.getWeather(db));
app.post('/registration', collection.findById(db));

//app.get('/remove', plantData.removeData(db))
//app.put('/registration',collection.deleteById(db));
//app.delete('/registration', collection.deleteById(db));
//app.get('/userList',routes.userList(db));
app.get("/ui",ui.renderUi(db) );
//app.listen(port);
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var request = require("request");

//var the_interval = 5 * 60 * 1000;  // Every 5 min.
//setInterval(checkThings, the_interval);

function checkThings(){

    console.log("CheckThings");
    request('http://localhost:3000/updateThings', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body) // Show the HTML for the Google homepage.
        }
    });

  }
