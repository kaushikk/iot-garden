function prettyDate(dateString){
    //if it's already a date object and not a string you don't need this line:
    var date = new Date(dateString);
    var d = date.getDate("hh:mm:ss");
    var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
    var m = monthNames[date.getMonth()];
    var y = date.getFullYear();
    var hr= date.getHours();
     var min= date.getMinutes();
     var sec= date.getSeconds();
    return d+' '+m+' '+y+' '+hr+':'+min+':'+sec;
}

exports.insertPlants = function(db){

return function(req, res) {
	dataCollection=db.get('irrigatorData');
	    
	        var data= req.body;
	        
	        
	         data.insertTime=prettyDate(Date.now());
	           
	         dataCollection.insert(data, function(err, result) {
	                  if (err) {
	                      res.send({'error':'An error has occurred'+err});
	                   } else {
	                      console.log('New Data has been added');
	                      
	                  }
	              });
	           
	        
	        
	    dataCollection.find({},{},function(e,docs){

	        var jsonObj = {'details':'value'};
	        jsonObj['details']=docs;
	        
	        res.send(JSON.stringify(jsonObj));
	    });
	   
	  };
	};


	exports.removeData = function(db){

	return function(req, res) {
		dataCollection=db.get('irrigatorData');
	    

	    	  dataCollection.remove({'deviceId':2} , {safe:true}, function(err, result) {
        		    if (err) {
         		       res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
               // res.writeHead('200');
                res.send({msg:result});
            }
        });

	};
};