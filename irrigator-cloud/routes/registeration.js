
/*
 * Register New Thing to the Network
 */

function prettyDate(dateString){
    //if it's already a date object and not a string you don't need this line:
    var date = new Date(dateString);
    var d = date.getDate("hh:mm:ss");
    var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
    var m = monthNames[date.getMonth()];
    var y = date.getFullYear();
    var hr= date.getHours();
     var min= date.getMinutes();
     var sec= date.getSeconds();
    return d+' '+m+' '+y+' '+hr+':'+min+':'+sec;
}
exports.findById = function(db){

return function(req, res) {

    
    var thing = req.body;
  //  console.log(thing);
    var id = thing.deviceId;
    
    var formattedDate = prettyDate(Date.now());
  //  console.log(id);
    console.log('Retrieving Data for Thing: ' + id);
    thing.insertTime=formattedDate;
    
    thing.isActive=true;
    collection=db.get('irrigator');
    
    collection.findOne({'deviceId':id}, function(err, item) {

            if(item==null){
               collection.insert(thing, function(err, result) {
                  if (err) {
                      res.send({'error':'An error has occurred'+err});
                   } else {
                      console.log('New Thing has been added');
                      
                  }
              });
            }
            else{

              collection.update({'deviceId':id} , thing, {safe:true}, function(err, result) {
                        if (err) {
                            res.send({'error':'An error has occurred - ' + err});
                        } else {
                            console.log('Thing :' + id+ ' document updated as inActive');
                           // res.writeHead('200');
                            
                        }
                  });
            }

    });
    collection.find({},{},function(e,docs){
        var jsonObj = {'details':'value'};
        jsonObj['details']=docs;
        
        res.send(JSON.stringify(jsonObj));
    });
   
  };

};
