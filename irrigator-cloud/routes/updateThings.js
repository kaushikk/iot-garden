
/*
 * Update Things to the Network
 */
exports.updateById = function(db){

    return function(req, res) {
        var collection = db.get('irrigator');
        var msg="Check Things Communication with cloud";
        var currentTime=Math.floor(Date.now() / 1000);
        collection.find({},{}).each(function(doc){
               // handle doc
               if(currentTime-doc.ttl >=303){
                  console.log("Old Document :"+doc.deviceId);

                  doc.isActive=false;
                  collection.update({'deviceId':doc.deviceId} , doc, {safe:true}, function(err, result) {
                        if (err) {
                            res.send({'error':'An error has occurred - ' + err});
                        } else {
                            console.log('Thing :' + doc.deviceId + ' document updated as inActive');
                           // res.writeHead('200');
                            
                        }
                  });

               }
               
        })
            .error(function(err){
               // handle error
            })
            .success(function(){
              // final callback
              res.send(msg);
          });
    };

  };
