
exports.getWeather = function(db){

return function(req, res) {
		var YQL = require('yql');

		var query = new YQL('select * from weather.forecast where (location = 95051)');

		query.exec(function(err, data) {
		  var location = data.query.results.channel.location;
		  var condition = data.query.results.channel.item;
		  
		  console.log('The current weather in ' + location.city);

		  res.send(condition);
		});
	};
};