#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <util.h>
#include <sensor.h>

String registerUrl = "http://172.20.96.221:3000/registration";
// String registerUrl = "http://172.20.96.221:8000/registration";
const char* ssid="BroncoWiFi";
const char* pwd="gosantaclara";

bool _debug=true;
Util util(_debug);
// Webserver , Local IP Address
IPAddress ip;
ESP8266WebServer server(80);
//Buffer of
StaticJsonBuffer<4096> _devBuff;
Thing _self;
sensor _sensor(_debug);

void connectCloud() {
    StaticJsonBuffer<400> jb;
    JsonObject& o = jb.createObject();
    o["deviceId"]=_self.getId();
    o["localIP"]=_self.getIP();
    o["mac"]=_self.getMac();
    JsonObject& cap = o.createNestedObject("capabilities");
    if(_self.isTemp()==true) {
        JsonObject& temp = cap.createNestedObject("temperature");
        temp["remoteControl"]="false";
        temp["threshold"]="false";
    }
    if(_self.isWater()==true) {
        JsonObject& temp = cap.createNestedObject("waterlevel");
        temp["remoteControl"]="false";
        temp["threshold"]="false";
    }
    if(_self.isSoil()==true) {
        JsonObject& temp = cap.createNestedObject("soil");
        temp["remoteControl"]="false";
        temp["threshold"]="false";
    }
    String payload;
    o.printTo(payload);
    util.debug("device info: ");
    o.prettyPrintTo(Serial);
    //How to store this information in a varaiable? and access in loop method?
    JsonObject& deviceList = util.sendJson(registerUrl,payload, _devBuff);
    if(deviceList.measureLength()>0) {
        util.debug("deviceList: ");
        deviceList.prettyPrintTo(Serial);
        JsonArray& details = deviceList.get("details");
        int size = details.size();
    }else {
        util.debug("error getting device List");
    }
}

void getWaterLevel() {
    int water = _sensor.readwaterlevel();
    Serial.println("water level: "+water);
    server.send(200, "text/plain", ""+String(water)+"\r\n\r\n");
}
void getTemperature() {
    float temp = _sensor.readtempreture();
    // float val=90;
    Serial.println("temperature");
    Serial.println(temp);
    server.send(200, "text/plain", String(temp)+"\r\n\r\n");
}

void handleNotFound() {
    server.send(404,"text/plain","URL Not Found");
}

void handleDefault() {
    server.send(200,"text/plain","Temperature Sensor...");
}

void setupServer() {
    if(_self.isWater()==true) {
        server.on("/water/get",getWaterLevel);
    }else if(_self.isTemp()==true) {
        server.on("/temp/get",getTemperature);
    }
    Serial.println("Add default handlers...");
    server.onNotFound(handleNotFound);
    server.on("/",handleDefault);
    server.begin();
}

void setup() {
    Serial.begin(115200);
    util.debug("starting iot...temperature-sensor...");
    ip = util.connectWifi(ssid, pwd);
    _self = Thing(THING_TEMPERATURE,ip.toString(),THING_TEMPERATURE);
    _self.setMac(util.getMac());
    connectCloud();
    setupServer();
    //server = util.startWebServer();
}

void loop() {
    util.debug("temperature-sensor...."+_self.getIP());
    server.handleClient();
     delay(500);
}
